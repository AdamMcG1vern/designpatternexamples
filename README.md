### What is this repository for? ###

This repository contains the code samples created for Guidewire's Design Pattern workshop. 

### How do I get set up? ###

Relatively simply; pull the repository down and simply open in the Editor of your choice(I recommend Intellij, as the repo contains the Workspace configuration files). 

### Contribution guidelines ###

When contributing, please ensure you create a pull request and add either Adam McGivern or Michael Heneghan as reviewers. 

### Who do I talk to? ###

If there's any issues, please contact either Adam McGivern or Michael Heneghan; both of these people will have Admin rights to the Repo. 