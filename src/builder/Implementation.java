package builder;

import java.util.Arrays;

/**
 * Created by mheneghan on 15/05/2018.
 */
public class Implementation {

    AnimalBuilder duckBuilder = new AnimalBuilder()
        .setAge(4)
        .setFavouriteFoods(Arrays.asList("grass", "fish"))
        .setSpecies("duck");
    Animal duck = duckBuilder.build();
}
