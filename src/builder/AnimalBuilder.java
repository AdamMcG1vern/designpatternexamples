package builder;

import java.util.List;

/**
 * Created by mheneghan on 15/05/2018.
 */
public class AnimalBuilder {
    private String species;
    private int age;
    private List<String> favouriteFoods;

    public AnimalBuilder setAge(int age){
        this.age = age;
        return this;
    }

    public AnimalBuilder setSpecies(String species){
        this.species = species;
        return this;
    }

    public AnimalBuilder setFavouriteFoods(List<String> favouriteFoods){
        this.favouriteFoods = favouriteFoods;
        return this;
    }

    public Animal build(){
        return new Animal(species, age, favouriteFoods);
    }
}
