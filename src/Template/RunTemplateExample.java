package Template;


import Template.MVR.MVRResponse;

public class RunTemplateExample {

    public static void main(String[] args) {
        MVRPlugin<MVRResponse> myPlugin;
        MVRResponse response = new MVRResponse(205, "Adam McGivern");

        //One Template implementation
        myPlugin = new BaseMVRPlugin();
        System.out.println(myPlugin.orderMVRScore());

        //Another, modified version
        myPlugin = new BaseMVRPluginML();
        System.out.println(myPlugin.orderMVRScore());
    }
}
