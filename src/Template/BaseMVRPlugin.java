package Template;

import Template.MVR.MVRResponse;

public class BaseMVRPlugin extends MVRPlugin<MVRResponse> {

    @Override
    public MVRResponse makeRequest() {
        return new MVRResponse(205, "Adam McGivern");
    }

    public int handleResponse(MVRResponse response) {
        return response.getScore() * 2 * 3;
    }
}

