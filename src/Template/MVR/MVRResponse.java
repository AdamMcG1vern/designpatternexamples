package Template.MVR;

public class MVRResponse {

    private int score;
    private String name;

    public MVRResponse(int responseScore, String responseName) {
        setScore(responseScore);
        setName(responseName);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
