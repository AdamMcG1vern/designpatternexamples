package Template;

import Template.MVR.MVRResponse;

public class BaseMVRPluginML extends MVRPlugin<MVRResponse> {
    private int accessCounter;

    @Override
    public MVRResponse makeRequest() {
        return new MVRResponse(455, "Bob Dylan");
    }

    public int handleResponse(MVRResponse response) {
        logAccess();
        return response.getScore() / 3 * 5;
    }

    private void logAccess () {
        ++accessCounter;
        System.out.println("MVR has been called: " + accessCounter + " Times.");
    }
}
