package Template;

import Template.MVR.MVRResponse;

public abstract class MVRPlugin<E> {
    final int orderMVRScore() {
        E mvrResponse = makeRequest();
        int score = handleResponse(mvrResponse);
        return score;
    }

    public abstract E makeRequest ();

    public abstract int handleResponse(E response);
}


