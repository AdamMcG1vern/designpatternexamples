package Adapter;

public interface AdapterInterface {

    public void parse(String myString);
}
