package Adapter.objectadapter;

import Adapter.AdapterInterface;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//Delegation for Adapter happens at RUNTIME here
public class StringIntoNumbersAdapter implements AdapterInterface {
    private Summate parser;
    private static int counter;

    public StringIntoNumbersAdapter(Summate newParser) {
        parser = newParser;
    }

    @Override
    public void parse(String myString) {
        //Adapt the passed in String to an int array
        try {
            String[] myStrings = myString.split("\\B");
            Stream<String> streamToForString = Arrays.stream(myStrings);
            List<Integer> numericValue = streamToForString.map(number -> Integer.valueOf(number)).collect(Collectors.toList());
            int[] numbersToSummate = numericValue.stream().mapToInt(number -> number).toArray();
            //call the sum function on Summate to summate the integers
            int summatedValue = parser.sum(numbersToSummate);
            System.out.println("Summation result: " + summatedValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
