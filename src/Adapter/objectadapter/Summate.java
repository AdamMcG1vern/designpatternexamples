package Adapter.objectadapter;

import java.util.Arrays;

public class Summate {
    public int sum(int[] characters) {
        return Arrays.stream(characters).sum();
    }
}
