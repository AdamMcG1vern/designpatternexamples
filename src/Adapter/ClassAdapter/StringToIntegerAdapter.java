package Adapter.ClassAdapter;

        import Adapter.AdapterInterface;

//Delegation of Adapter happens at COMPILE TIME
public class StringToIntegerAdapter extends Calculate implements AdapterInterface {

    @Override
    public void parse(String myString) {
        try {
            //Convert String to integer
            int parsedValue = Integer.parseInt(myString);
            //Call the SUPERCLASS calculate function
            int calculatedValue = calculate(parsedValue);

            System.out.println("Multiplied By Four: " + calculatedValue);
        } catch (NumberFormatException parser) {
            parser.printStackTrace();
        }
    }
}
