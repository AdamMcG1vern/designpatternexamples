package Adapter;

import Adapter.ClassAdapter.StringToIntegerAdapter;
import Adapter.objectadapter.StringIntoNumbersAdapter;
import Adapter.objectadapter.Summate;

public class RunAdapterExample {

    public static void main(String[] args) {
       //Object Adapter using composition
        Summate upperCaseConverter = new Summate();
       StringIntoNumbersAdapter objectAdapter = new StringIntoNumbersAdapter(upperCaseConverter);
       objectAdapter.parse("12345");


       //Class adapter using inheritance.
       StringToIntegerAdapter calculateAdapter = new StringToIntegerAdapter();
       calculateAdapter.parse("123432");
    }
}
