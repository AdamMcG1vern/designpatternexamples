package Singleton.ThreadSafeSingleton;
//Thread safe, double locking version of the Lazy Initialisation Singleton Pattern
public class SingletonWithDoubleCheckedLocking {
    //Volatile Keyword ensures that this variable, when used, is threadsafe
    private volatile static SingletonWithDoubleCheckedLocking ourInstance;

    public static SingletonWithDoubleCheckedLocking getInstance() {
        if (ourInstance == null) {
            synchronized (ourInstance) {
                if (ourInstance == null) {
                    ourInstance = new SingletonWithDoubleCheckedLocking();
                }
            }
        }
        return ourInstance;
    }
}
