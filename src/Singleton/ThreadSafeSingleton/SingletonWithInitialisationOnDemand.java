package Singleton.ThreadSafeSingleton;
//This is an advanced version of singleton which uses the Initialisation-On-Demand Pattern Idiom: https://en.wikipedia.org/wiki/Initialization-on-demand_holder_idiom
public class SingletonWithInitialisationOnDemand {
    //Utilises quirk of JVM to trivalise initialise this class, but only actually initialise inner classes when they need to be executed- hence it is Lazy Initialised.
    //Also, the initialisation of the static inner class will be sequential in nature, and as such inheritently thread safe. 
    private static class DBConnectionLoader {
        static final SingletonWithInitialisationOnDemand INSTANCE = new SingletonWithInitialisationOnDemand();
    }

    public static SingletonWithInitialisationOnDemand getInstance() {
        return DBConnectionLoader.INSTANCE;
    }

    private SingletonWithInitialisationOnDemand() {
    }
}
