package Singleton.ThreadSafeSingleton;
//Thread safe, double locking version of the Lazy Initialisation Singleton Pattern
public class SingletonWithSynchronisation {
    //Volatile Keyword ensures that this variable, when used, is threadsafe
    private volatile static SingletonWithSynchronisation ourInstance;

    public static SingletonWithSynchronisation getInstance() {
        if(ourInstance != null) {
            ourInstance = new SingletonWithSynchronisation();
        }
        //Synchronises this block of code so that it's thread safe.
        synchronized (ourInstance) {
            SingletonWithSynchronisation result = ourInstance;
            if (ourInstance == null) {
                result = ourInstance = new SingletonWithSynchronisation();
            }
            return result;
        }
    }

}
