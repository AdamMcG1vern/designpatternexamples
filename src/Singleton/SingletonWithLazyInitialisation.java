package Singleton;

//Implementation of Singleton with Lazy Initialisation. 
public class SingletonWithLazyInitialisation {
    private static SingletonWithLazyInitialisation INSTANCE;

    public static SingletonWithLazyInitialisation getInstance() {
        //Initialises the instance only when this function is called for the first time.
        if (INSTANCE == null) {
        INSTANCE = new SingletonWithLazyInitialisation();
        }
        return INSTANCE;
    }

    private SingletonWithLazyInitialisation() {
    }
}