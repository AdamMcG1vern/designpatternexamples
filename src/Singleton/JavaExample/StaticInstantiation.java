package Singleton.JavaExample;

/**
 * Created by mheneghan on 30/04/2018.
 */
public class StaticInstantiation {
    private static final StaticInstantiation instance;

    static {
        instance = new StaticInstantiation();
        // Perform additional tasks
    }

    private StaticInstantiation(){
    }

    public static StaticInstantiation getInstance(){
        return instance;
    }

    // Data access methods

}
