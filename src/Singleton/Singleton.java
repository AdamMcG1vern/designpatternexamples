package Singleton;
//A Simple implementation of the Singleton Pattern - Initialisation is eager and this is NOT thread Safe. 
public class Singleton {
    private static Singleton ourInstance = new Singleton();

    public static Singleton getInstance() {
        return ourInstance;
    }

    private Singleton() {
    }
}
