package Facade;

import Facade.Integrations.PolicyPeriod;

public class RunFacadeExample {

    public static void main(String[] args) {
        QuoteIntegrations quoteIntegration = new QuoteIntegrations();
        PolicyPeriod samplePolicyPeriod = new PolicyPeriod();

        quoteIntegration.start(samplePolicyPeriod);
    }
}
