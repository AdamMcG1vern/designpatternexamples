package Facade.Integrations;

public class Clue {

    public void orderClue(PolicyPeriod period) {
        System.out.println("CLUE has been ordered and applied to this period");
    }
}