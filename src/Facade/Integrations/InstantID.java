package Facade.Integrations;

public class InstantID {

    public void orderInstantID(PolicyPeriod period) {
        System.out.println("InstantID has been ordered and applied to period");
    }
}