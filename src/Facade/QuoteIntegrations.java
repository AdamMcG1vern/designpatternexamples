package Facade;

import Facade.Integrations.Clue;
import Facade.Integrations.InstantID;
import Facade.Integrations.PolicyPeriod;

public class QuoteIntegrations {
    private InstantID instantIDInstance;
    private Clue clueInstance;

    public QuoteIntegrations () {
        instantIDInstance = new InstantID();
        clueInstance = new Clue();
    }

    public void start (PolicyPeriod period) {
        try {
        clueInstance.orderClue(period);
        instantIDInstance.orderInstantID(period);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}