package factory;

/**
 * Created by mheneghan on 16/05/2018.
 */
public class ZooKeeper {

    public static void main(String[] args) {
        final Food food = FoodFactory.getFood("polar bear");
        food.consumed();
    }
}
