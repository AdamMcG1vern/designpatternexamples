package factory;

/**
 * Created by mheneghan on 16/05/2018.
 */

import factory.Food;
public class FoodFactory {

    public static Food getFood(String animalName){
        switch (animalName){
            case "zebra": return Food.new Hay(100);
            case "rabbit": return new Food.Pellets(5);
            case "goat": return new Food.Pellets(30);
            case "polar bear": return new Food.Fish(10);
        }
        throw new UnsupportedOperationException("Unsupported animal: " + animalName);
    }

}
