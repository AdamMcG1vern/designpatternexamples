package Proxy;

import java.util.ArrayList;

public class RunProxyExample {

    public static void main(String[] args) {
        ArrayList<String> uwIssues = new ArrayList<String>();
        uwIssues.add("The driver is too young");

        Quote testFailureQuote = new Quote(uwIssues);
        Quote testPassQuote = new Quote( new ArrayList<String>());

        ProxyBindProcess bind = new ProxyBindProcess();
        System.out.println(bind.BindAndIssueQuote(testFailureQuote));
        System.out.println(bind.BindAndIssueQuote(testPassQuote));
    }
}
