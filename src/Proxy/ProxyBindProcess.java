package Proxy;

public class ProxyBindProcess implements IBindProcess {
    private DefaultBindProcess bindProcess;

    public ProxyBindProcess () {
        bindProcess = new DefaultBindProcess();
    }


    @Override
    public String BindAndIssueQuote(Quote quote) {
        if (quote.IsValid()) {
           return bindProcess.BindAndIssueQuote(quote);
        }
        return "Unable to bind Quote";
    }
}
