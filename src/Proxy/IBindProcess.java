package Proxy;

public interface IBindProcess {

    public String BindAndIssueQuote(Quote quote);

}
