package Proxy;

import java.util.ArrayList;

public class Quote {
    private ArrayList<String> underwritingIssues;

    public Quote (ArrayList<String> uwIssues) {
        underwritingIssues = uwIssues;
    }

    public boolean IsValid(){
        return underwritingIssues.size() == 0;
    }

    private void SetUnderwritingIssues(ArrayList<String> uwIssues){
        underwritingIssues = uwIssues;
    }

    private ArrayList<String> GetUnderwritingIssues(){
        return underwritingIssues;
    }
}
