package immutable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mheneghan on 15/05/2018.
 */
public class Animal {
    private String species;
    private int age;
    private final List<String> favouritFoods;

    public Animal(String species, int age, List<String> favouritFoods) {
        this.species = species;
        this.age = age;
        if (favouritFoods == null) {
            throw new RuntimeException("favouriteFoods is required");
        }
        this.favouritFoods = new ArrayList<String>(favouritFoods);
    }

    public List<String> getFavouritFoodsIncorrectly() { // MAKES CLASS MUTABLE

        return favouritFoods;
    }

    public List<String> getFavouritFoodscorrectly() { // MAKES CLASS IMMUTABLE

        List<String> copy = new ArrayList<>(favouritFoods);

        return copy;
    }
    public String getFavouritFood(int index) {

        return favouritFoods.get(index);
    }

    public int getFavouritFoodsCount() {

        return favouritFoods.size();
    }

    public String getSpecies() {

        return species;
    }

    public int getAge() {

        return age;
    }

}
